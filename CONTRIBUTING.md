Comment contribuer ?
====================
# Je suis développeur je veux apporter mon aide

En local (via git)
------------------
![Git](http://ncredinburgh.com/blog/posts/img/git-less-know-gems/git-logo.png)
Tout d'abord vous devez posséder le gestionnaire de version git.
### C'est quoi un gestionnaire de version ?
C'est un logiciel qui permet de stocker l'ensemble des fichiers d'un projet par exemple en conservant la chronologie de toute les modification qui ont été effectuées dessus.
### Comment installer git ?
#### Sous GNU/Linux
##### Debian et Ubuntu
```
apt-get install git
```
##### Fedora
```
yum install git
```
##### Gentoo
```
emerge --ask --verbose dev-vcs/git
```
##### Arch Linux
```
pacman -S git 
```
##### openSUSE
```
zypper install git
```
#### Sous BSD
##### FreeBSD
```
cd /usr/ports/devel/git
make install
```
##### OpenBSD
```
pkg_add git
```
#### Sous Microsoft Windows
Télécharger l'executable sur le site : https://git-for-windows.github.io/.

Installé le.

#### Sous Mac
Télécharger l'installateur graphique sur le site : http://sourceforge.net/projects/git-osx-installer/.

Installé le.

### Comment récupérer les sources du site web via git ?
```
git clone https://framagit.org/hemery/LaViBi.git
```
Via GitLab
----------
Sous GitLab vous pouvez contribuez en commencant par forker le projet.
# Je veux simplement apporter des idées d'amélioration du site
Vous pouvez proposer des idées d'améliration du site via l'onglet Issues du dépot git. De clique sur l'icone new issue et de nommer votre issue.